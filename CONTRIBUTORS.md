## Technical Steering Committee

TSC Members roles and election are defined in the Charter here : https://gitlab.com/sylva-projects/sylva/-/blob/main/Project_Sylva_Technical_Charter.pdf

| company | Members | Alternates |
|---------|---------|------------|
| Orange | Guillaume Nevicato  | Mathieu Rohon |
| DT | Nathan Rader | Kai Steuernagel |
| Vodafone | Nikoleta Patroni | Amy Lu |
| TIM | Carlo Cavazzoni | Alessandro Capello |
| TEF | Luis Velarde | Javier Ramon Salguero |
| Nokia | Hamalainen, Jouni M | Thoralf Czichy |
| Ericsson | Norbert Niebert | Rihab Banday |

## TSC CoChair
@GuillaumeNevicato and @Carlo-TIM are CoChairs of the TSC

## Committers

| Workgroup | company | Name |
|---------|---------|------------|
| WG01 | Orange  | Mathieu Rohon |
| WG01 | Orange  | François Eleouet |
| WG01 | Orange  | Thomas Morin |
| WG01 | Orange  | Bogdan Burciu |
| WG01 | Orange  | Cristian Manda |
| WG01 | TIM  | Alessandro Capello|
| WG02 | TEF  | Luis Velarde |
| WG03 | Orange  | Theophile Debauche |

WG01 : Cloud Stack Development
WG02 : Validation Center
WG03 : Security Requirements

## Contributors
Coming soon
