# Sylva Governance

This repository contains the governance documents for [Sylva](https://www.linuxfoundation.org/press/linux-foundation-europe-announces-project-sylva-to-create-open-source-telco-cloud-software-framework-to-complement-open-networking-momentum).

## Organization

According to the [Technical Charter](https://gitlab.com/sylva-projects/governance/-/blob/main/Project_Sylva_Technical_Charter.pdf) of the project, Sylva runs under the lead of the Technical Steering Committee (TSC). The TSC is responsible for all technical oversight of the open source project.

All TSC meetings are public. The minutes of the TSC meetings can be found on [the wiki](https://gitlab.com/sylva-projects/governance/-/wikis/home).

## How to contribute

In addition to the TSC, according to the Technical Charter of the project, there are two types of contributors :
* "**Contributors**" include anyone in the technical community that contributes code, documentation, or other technical artifacts to the Sylva project. [Link to contributors.md](https://gitlab.com/sylva-projects/governance/-/blob/main/CONTRIBUTORS.md)
* "**Committers**" are Contributors who have earned the ability and have the responsibility to modify source code, documentation, or other technical artifacts in the Sylva project repositories (Committers have the ability to merge pull requests in the Sylva Gitlab repositories). A Contributor may become a Committer by a majority approval of the TSC members.

Contributors may submit pull requests to the Sylva repositories on Gitlab. The pull requests will be reviewed by at least one Committer who may decide to accept or reject the pull request.

We are working on guidance elements for Contributors. Those elements will be provided in a "CONTRIBUTING" file in this repository.

## License
All new inbound code contributions to the Project must be made using the Apache License, Version 2.0, available at https://www.apache.org/licenses/LICENSE-2.0 


